package cn.itcast.service;


import cn.itcast.domain.City;

import java.io.IOException;
import java.util.List;

public interface CityService {
    List<City> findCityByProvinceId(String ProvinceId) throws IOException;
}
