package cn.itcast.service.impl;

import cn.itcast.dao.CityDao;
import cn.itcast.domain.City;
import cn.itcast.service.CityService;
import cn.itcast.utils.JedisUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.util.StringUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.List;

public class CityServiceImpl implements CityService {
    @Override
    public List<City> findCityByProvinceId(String ProvinceId) throws IOException {
        //1.根据provinceId从redis中获取数据
        Jedis jedis = JedisUtils.getJedis();
        String citiesStr = jedis.hget("cities", ProvinceId);

        try {
            //2.1如果没有获取到，就从mysql中查询，然后缓存到redis中
            if (StringUtil.isEmpty(citiesStr)) {
                SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-conﬁg.xml")).openSession();
                CityDao cityDao = sqlSession.getMapper(CityDao.class);
                List<City> xx = cityDao.findCityByProvinceId(ProvinceId);
                sqlSession.close();

                jedis.hset("cities",ProvinceId,new ObjectMapper().writeValueAsString(xx));
                return xx;
            }else {//3.1如果查询到，就将数据转成List返回
                return new ObjectMapper().readValue(citiesStr,List.class);
            }
        } finally {
            JedisUtils.close(jedis);
        }
    }
}
