package cn.itcast.service.impl;

import cn.itcast.dao.ProvinceDao;
import cn.itcast.domain.Province;
import cn.itcast.service.ProvinceService;
import cn.itcast.utils.JedisUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.util.StringUtil;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import redis.clients.jedis.Jedis;

import java.util.List;

public class ProvinceServiceImpl implements ProvinceService {
    @Override
    public List<Province> findAll() throws Exception {
        Jedis jedis = JedisUtils.getJedis();
        String provinceStr = jedis.get("province");


        if(StringUtil.isEmpty(provinceStr)){
            SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-conﬁg.xml")).openSession();
            ProvinceDao provinceDao = sqlSession.getMapper(ProvinceDao.class);
            List<Province> provinces = provinceDao.findAll();
            sqlSession.close();

            jedis.set("provinces",new ObjectMapper().writeValueAsString(provinces));
            JedisUtils.close(jedis);
            return provinces;


        }else{
            JedisUtils.close(jedis);
            return new ObjectMapper().readValue(provinceStr,List.class);
        }
    }
}
