package cn.itcast.controller;

import cn.itcast.domain.City;
import cn.itcast.domain.Province;
import cn.itcast.service.impl.CityServiceImpl;
import cn.itcast.service.impl.ProvinceServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/PCServlet")
public class PCServlet extends BaseServlet   {

    private void findCityByProvinceId(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<City> cities = new CityServiceImpl().findCityByProvinceId(request.getParameter("provinceId"));
        response.getWriter().println(new ObjectMapper().writeValueAsString(cities));
    }

    private void findAllProvince(HttpServletRequest request,HttpServletResponse response) throws Exception {
        List<Province> provinces = new ProvinceServiceImpl().findAll();
        response.getWriter().println(new ObjectMapper().writeValueAsString(provinces));
    }
}
