package cn.itcast.controller;

import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

@Log4j
public class BaseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            request.setCharacterEncoding("utf-8");
            response.setContentType("application/json;charset=utf-8");

            String operator = request.getParameter("operator");

            /*1.获取字节码文件*/
            Class clazz = this.getClass();
            /*2.获取Method对象*/
            Method method = clazz.getDeclaredMethod(operator, HttpServletRequest.class, HttpServletResponse.class);
            /*3.暴力反射*/
            method.setAccessible(true);

            log.info("开始执行"+method.getName()+"方法");

            /*4.通过method执行对象的方法 this.findAllProvince(request,response)*/
            method.invoke(this,request,response);
            log.info("开始执行"+method.getName()+"方法");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
