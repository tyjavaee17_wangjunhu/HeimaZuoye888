package cn.itcast.dao;


import cn.itcast.domain.City;
import org.apache.ibatis.annotations.Select;


import java.util.List;

public interface CityDao {
    List<City> findCityByProvinceId(String ProvinceId);
}
