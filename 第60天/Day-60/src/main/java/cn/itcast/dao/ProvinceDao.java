package cn.itcast.dao;

import cn.itcast.domain.Province;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProvinceDao {
    List<Province> findAll()throws Exception;
}
