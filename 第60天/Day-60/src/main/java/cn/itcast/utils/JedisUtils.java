package cn.itcast.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ResourceBundle;

public class JedisUtils {
    private JedisUtils() {}
    private static JedisPool jedisPool;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("redis");
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(Integer.parseInt(bundle.getString("redis.maxTotal")));
        jedisPoolConfig.setMaxIdle(Integer.parseInt(bundle.getString("redis.maxIdle")));

        jedisPool = new JedisPool(jedisPoolConfig, bundle.getString("redis.host"), Integer.parseInt(bundle.getString("redis.port")));

    }

    /**从连接池获取jedis对象
     *
     * @return
     */
    public static Jedis getJedis(){
        return jedisPool.getResource();
    }

    /**
     *
     * @param jedis
     */
    public static void close(Jedis jedis){
        if(jedis!=null){
            jedis.close();
        }
    }
}
