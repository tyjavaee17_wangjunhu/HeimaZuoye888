package com.itheima.web.controller.store;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.store.Company;
import com.itheima.service.store.CompanySevice;
import com.itheima.service.store.impl.CompanySeviceImpl;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.system.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/store/company")
public class CommanyServlet extends BaseServlet {
    private CompanySevice companySevice = new CompanySeviceImpl();

    private void list(HttpServletRequest request, HttpServletResponse response) throws SecurityException, Exception {
        //进入列表页
        //获取数据
        int page = 1;
        int size = 5;
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (!StringUtils.isEmpty(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        PageInfo all = companySevice.findAll(page, size);
        //将数据保存到指定位置
        request.setAttribute("page", all);
        //跳转页面
        request.getRequestDispatcher("/WEB-INF/pages/store/company/list.jsp").forward(request, response);
    }

    private void toAdd(HttpServletRequest request, HttpServletResponse response) throws SecurityException, Exception {
        //跳转页面
        request.getRequestDispatcher("/WEB-INF/pages/store/company/add.jsp").forward(request,response);
    }

    private void save(HttpServletRequest request, HttpServletResponse response) throws SecurityException, Exception{
        //将数据获取到,封装成一个对象
        Company company = BeanUtil.fillBean(request, Company.class, "yyyy-MM-dd");
        //调用业务层接口sava
        companySevice.save(company);
        //跳转到页面LIst
        response.sendRedirect(request.getContextPath()+"/store/company?operation=list");
    }

    private void toedit(HttpServletRequest request, HttpServletResponse response) throws SecurityException, Exception{
        //查询要修改的数据findByid
        String id = request.getParameter("id");
        Company company = companySevice.findById("id");
        //将数据加载到指定区域,供页面获取
        request.setAttribute("company",company);
        //跳转页面
        request.getRequestDispatcher("/WEB-INF/pages/store/company/update.jsp").forward(request,response);
    }

    private void edit(HttpServletRequest request, HttpServletResponse response) throws SecurityException, Exception{
        //将数据捕捉到,封装成一个对象
        Company company = BeanUtil.fillBean(request, Company.class,"yyyy-MM-dd");
        //调用业务层接口save;\
        companySevice.update(company);
        //转回到页面list;
        response.sendRedirect(request.getContextPath()+"/store/company?operation=list");
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws SecurityException, Exception{
        //将数据获取到,封装成一个对象
        Company company = BeanUtil.fillBean(request, Company.class);
        //调用业务层接口save
        companySevice.delete(company);
        //转回到页面List
        response.sendRedirect(request.getContextPath()+"/store/company?operation=list");
    }
}

