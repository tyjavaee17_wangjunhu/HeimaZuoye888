package com.itheima.service.store;

import com.itheima.domain.store.QuestionItem;

import java.util.List;

public interface QuestionItemService {
    /**
     * 添加
     * @param questionItem
     * @return
     */
    void save(QuestionItem questionItem);

    /**
     * 删除
     * @param questionItem
     * @return
     */
    void delete(QuestionItem questionItem);

    /**
     * 修改
     * @param questionItem
     * @return
     */
    void update(QuestionItem questionItem);

    /**
     * 查询单个
     * @param id 查询的条件（id）
     * @return 查询的结果，单个对象
     */
    QuestionItem findById(String id);

    List<QuestionItem> findAll(String questionId);
}
