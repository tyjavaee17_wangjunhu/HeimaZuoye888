	#1) 建库
	CREATE DATABASE mydb DEFAULT CHARSET gbk;
	SHOW CREATE DATABASE mydb;
	USE mydb;
	SHOW TABLES;
	
	#2) 建表
	CREATE TABLE employee(
		id INT,
		NAME VARCHAR(20),
		sex VARCHAR(2),
		birthday DATE,
		email VARCHAR(10),
		remark VARCHAR(50)
	);
	
	 SHOW CREATE TABLE employee;
	 DESC employee;
	 
	# 3) 表的修改：
	ALTER TABLE employee ADD salary DOUBLE;
	ALTER TABLE employee MODIFY NAME VARCHAR(20);
	ALTER TABLE employee DROP email;
	ALTER TABLE employee CHANGE remark RESUME VARCHAR(50);
	
	CREATE TABLE student(
		id INT,
		NAME VARCHAR(20),
		chinese FLOAT,
		english FLOAT,
		math FLOAT
	);
	
	  #插入7条数据：张小明，李进，王五，李一，李来财，张进宝，黄蓉
	INSERT INTO student(id,NAME,chinese,english,math) VALUES(1,'张小明',89,78,90);
	INSERT INTO student(id,NAME,chinese,english,math) VALUES(2,'李进',67,53,95);
	INSERT INTO student(id,NAME,chinese,english,math) VALUES(3,'王五',87,78,77);
	INSERT INTO student(id,NAME,chinese,english,math) VALUES(4,'李一',88,98,92);
	INSERT INTO student(id,NAME,chinese,english,math) VALUES(5,'李来财',82,84,67);
	INSERT INTO student(id,NAME,chinese,english,math) VALUES(6,'张进宝',55,85,45);
	INSERT INTO student(id,NAME,chinese,english,math) VALUES(7,'黄蓉',75,65,30);

	#查询表中所有学生的信息。
	SELECT * FROM student;