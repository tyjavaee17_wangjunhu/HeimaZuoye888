package cn.itcast.controller;

import cn.itcast.service.impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/FindByPageServlet")
public class FindByPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");

        try{
            /*1.获取表单数据*/
            String _pageNum = request.getParameter("pageNum");
            if (StringUtil.isEmpty(_pageNum)) {
                _pageNum = "1";
            }
            String _pageSize = request.getParameter("pageSize");
            if (StringUtil.isEmpty(_pageSize)) {
                _pageSize = "2";
            }

            /*2.调用service*/
            PageInfo pi = new UserServiceImpl().findByPage(Integer.parseInt(_pageNum), Integer.parseInt(_pageSize));

            /*3.返回JSON数据*/
            response.getWriter().println(new ObjectMapper().writeValueAsString(pi));
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().println("服务器忙，请稍候访问");
        }
    }
}
