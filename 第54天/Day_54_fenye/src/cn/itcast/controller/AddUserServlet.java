package cn.itcast.controller;

import cn.itcast.domain.User;
import cn.itcast.service.impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddUserServlet")
public class AddUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");

        try{
            /*1.获取表单数据*/
            User user = new User();
            user.setPassword(request.getParameter("password"));
            user.setEmail(request.getParameter("email"));
            user.setBirthday(request.getParameter("birthday"));
            user.setName(request.getParameter("name"));

            /*2.调用service*/
            new UserServiceImpl().save(user);

            /*3.返回JSON数据*/
            response.getWriter().println("保存成功");
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().println("服务器忙，请稍候访问");
        }
    }
}
