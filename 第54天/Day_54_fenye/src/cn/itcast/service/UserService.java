package cn.itcast.service;

import cn.itcast.domain.User;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

public interface UserService {
    void save(User user) throws Exception;

    void update(User user) throws Exception;

    void deleteByPrimaryKey(Integer primaryKey) throws Exception;

    PageInfo findByPage(int pageNum,int pageSize) throws Exception;
}
