package cn.itcast.service.impl;

import cn.itcast.domain.User;
import cn.itcast.mapper.UserMapper;
import cn.itcast.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.util.List;

public class UserServiceImpl implements UserService {

    @Override
    public void save(User user) throws Exception {
        SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-conﬁg.xml")).openSession();
        sqlSession.getMapper(UserMapper.class).save(user);
        sqlSession.commit();
    }

    @Override
    public void update(User user) throws Exception {
        SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-conﬁg.xml")).openSession();
        sqlSession.getMapper(UserMapper.class).update(user);
        sqlSession.commit();
    }

    @Override
    public void deleteByPrimaryKey(Integer primaryKey) throws Exception {
        SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-conﬁg.xml")).openSession();
        sqlSession.getMapper(UserMapper.class).deleteByPrimaryKey(primaryKey);
        sqlSession.commit();
    }

    @Override
    public PageInfo findByPage(int pageNum, int pageSize) throws Exception {
        SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-conﬁg.xml")).openSession();

        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        PageHelper.startPage(pageNum,pageSize);
        List<User> list = userMapper.findAll();

        PageInfo<User> pi = new PageInfo<>(list);
        return pi;
    }
}
