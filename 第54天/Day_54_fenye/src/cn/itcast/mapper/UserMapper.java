package cn.itcast.mapper;

import cn.itcast.domain.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserMapper {
    @Select("select * from `user`")
    List<User> findAll()throws Exception;

    @Insert("insert into `user`(name,password,email,birthday) values(#{name},#{password},#{email},#{birthday})")
    void save(User user) throws Exception;

    @Update("update `user` set name=#{name}, password=#{password}, email=#{email}, birthday=#{birthday} where uid=#{uid}")
    void update(User user) throws Exception;

    @Delete("delete from `user` where uid = #{uid}")
    void deleteByPrimaryKey(Integer primaryKey) throws Exception;
}
