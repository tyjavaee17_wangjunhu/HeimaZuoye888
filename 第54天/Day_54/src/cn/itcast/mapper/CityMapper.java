package cn.itcast.mapper;

import cn.itcast.doamin.City;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CityMapper {
    @Select("select * from city where name like #{name} limit 0,5")
    List<City> findByCondition(String name)throws Exception;
}
