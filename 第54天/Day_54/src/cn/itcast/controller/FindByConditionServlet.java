package cn.itcast.controller;

import cn.itcast.doamin.City;
import cn.itcast.service.impl.CityServiceImpl;
import com.sun.javafx.css.StyleCache;
import sun.awt.SunHints;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Key;
import java.util.List;

@WebServlet("/FindByConditionServlet")
public class FindByConditionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");

        try {

        //调用表单数据
            String key = request.getParameter("key");
            //调用service
            List<City> list = new CityServiceImpl().findByCondion(key);
        }catch (Exception e) {
            e.printStackTrace();
            response.getWriter().println("服务器忙,请稍后访问");
        }
    }
}
