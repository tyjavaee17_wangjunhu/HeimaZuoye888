package cn.itcast.doamin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class City {
    private Integer _id;
    private String name;
    private Integer city_id;
    private Integer province_id;

}
