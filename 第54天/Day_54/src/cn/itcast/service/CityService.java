package cn.itcast.service;

import cn.itcast.doamin.City;

import java.util.List;

public interface CityService {
  List<City> findByCondion(String name)throws Exception;
}
