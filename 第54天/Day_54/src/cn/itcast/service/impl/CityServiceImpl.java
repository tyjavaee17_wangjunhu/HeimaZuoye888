package cn.itcast.service.impl;

import cn.itcast.doamin.City;
import cn.itcast.mapper.CityMapper;
import cn.itcast.service.CityService;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.SQL;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.util.List;

public class CityServiceImpl implements CityService {
    @Override
    public List<City> findByCondion(String name) throws Exception {
        SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml")).openSession();
        List<City> list = sqlSession.getMapper(CityMapper.class).findByCondition("%" + name + "%");
        return list;
    }
}
