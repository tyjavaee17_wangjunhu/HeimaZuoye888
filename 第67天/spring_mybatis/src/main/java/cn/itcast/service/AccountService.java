package cn.itcast.service;

import cn.itcast.domain.Account;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AccountService {
    List<Account> findAll();
}
