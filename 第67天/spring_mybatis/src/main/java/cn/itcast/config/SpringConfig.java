package cn.itcast.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("cn.itcast")
@Import({JdbcConfig.class,MyBatisConfig.class})
public class SpringConfig {
}
