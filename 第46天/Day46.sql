-- 创建今天的数据库
CREATE DATABASE day21;

-- 选中数据库
USE day21;

-- 准备数据
-- 部门表
CREATE TABLE dept (
  id INT PRIMARY KEY, -- 部门id
  dname VARCHAR(50), -- 部门名称
  loc VARCHAR(50) -- 部门所在地
);

-- 添加4个部门
INSERT INTO dept(id,dname,loc) VALUES 
(10,'教研部','北京'),
(20,'学工部','上海'),
(30,'销售部','广州'),
(40,'财务部','深圳');

-- 职务表，职务名称，职务描述
CREATE TABLE job (
  id INT PRIMARY KEY,
  jname VARCHAR(20),
  description VARCHAR(50)
);

-- 添加4个职务
INSERT INTO job (id, jname, description) VALUES
(1, '董事长', '管理整个公司，接单'),
(2, '经理', '管理部门员工'),
(3, '销售员', '向客人推销产品'),
(4, '文员', '使用办公软件');

-- 员工表
CREATE TABLE emp (
  id INT PRIMARY KEY, -- 员工id
  ename VARCHAR(50), -- 员工姓名
  job_id INT, -- 职务id
  mgr INT , -- 上级领导
  joindate DATE, -- 入职日期
  salary DECIMAL(7,2), -- 工资
  bonus DECIMAL(7,2), -- 奖金
  dept_id INT, -- 所在部门编号
  CONSTRAINT emp_jobid_ref_job_id_fk FOREIGN KEY (job_id) REFERENCES job (id),
  CONSTRAINT emp_deptid_ref_dept_id_fk FOREIGN KEY (dept_id) REFERENCES dept (id)
);

-- 添加员工
INSERT INTO emp(id,ename,job_id,mgr,joindate,salary,bonus,dept_id) VALUES 
(1001,'孙悟空',4,1004,'2000-12-17','8000.00',NULL,20),
(1002,'卢俊义',3,1006,'2001-02-20','16000.00','3000.00',30),
(1003,'林冲',3,1006,'2001-02-22','12500.00','5000.00',30),
(1004,'唐僧',2,1009,'2001-04-02','29750.00',NULL,20),
(1005,'李逵',4,1006,'2001-09-28','12500.00','14000.00',30),
(1006,'宋江',2,1009,'2001-05-01','28500.00',NULL,30),
(1007,'刘备',2,1009,'2001-09-01','24500.00',NULL,10),
(1008,'猪八戒',4,1004,'2007-04-19','30000.00',NULL,20),
(1009,'罗贯中',1,NULL,'2001-11-17','50000.00',NULL,10),
(1010,'吴用',3,1006,'2001-09-08','15000.00','0.00',30),
(1011,'沙僧',4,1004,'2007-05-23','11000.00',NULL,20),
(1012,'李逵',4,1006,'2001-12-03','9500.00',NULL,30),
(1013,'小白龙',4,1004,'2001-12-03','30000.00',NULL,20),
(1014,'关羽',4,1007,'2002-01-23','13000.00',NULL,10);

-- 工资等级表
CREATE TABLE salarygrade (
  grade INT PRIMARY KEY,   -- 级别
  losalary INT,  -- 最低工资
  hisalary INT -- 最高工资
);

-- 添加5个工资等级
INSERT INTO salarygrade(grade,losalary,hisalary) VALUES 
(1,7000,12000),
(2,12010,14000),
(3,14010,20000),
(4,20010,30000),
(5,30010,99990);


/********************多表查询综合案例：练习1 *********************/
-- 查询所有员工姓名，工资，工资等级
-- 1、确定查询哪些表
-- 分析：员工姓名，工资 emp表
--       工资等级 salarygrade
      
SELECT * FROM emp e
 INNER JOIN salarygrade s 
-- 2、确定连接条件
-- 分析： emp表与salarygrade没有外键关系
--        emp表中员工的薪水salary 与 salarygrade表中等级的低薪水与高薪水时有关系
-- 得出连接条件：  员工的薪水必须在对应等级的薪水范围内
SELECT * FROM emp e
 INNER JOIN salarygrade s ON e.salary BETWEEN s.losalary AND s.hisalary;
 -- 注意：多表查询连接条件99%的情况下是 主表.主键=从表.外键，但是1%并不是，需要根据业务找关系

-- 3、确定查询的列
-- 所有员工姓名，工资，工资等级
SELECT e.ename 姓名,e.salary 工资,s.grade 工资等级 FROM emp e
 INNER JOIN salarygrade s ON e.salary BETWEEN s.losalary AND s.hisalary;
 
-- 上面是内连接：不一定会所有员工都会出来，需要使用外连接才能保证所有员工出来，
-- 分析：外连接要以员工表为主，所以下面使用左外连接，员工表是左表
SELECT e.ename 姓名,e.salary 工资,s.grade 工资等级 FROM emp e
 LEFT JOIN salarygrade s ON e.salary BETWEEN s.losalary AND s.hisalary;
 
 /********************多表查询综合案例：练习2 *********************/
 -- 查询经理的信息。显示经理姓名，工资，职务名称，部门名称，工资等级
-- 1、确定查询哪些表
-- 分析：显示经理姓名，工资： emp表
--       职务名称： job表
--       部门名称： dept表
--       工资等级： salarygrade表
-- 2、确定连接条件      
SELECT * FROM emp e
 INNER JOIN job j ON e.job_id = j.id
 INNER JOIN dept d ON e.dept_id = d.id
 INNER JOIN salarygrade s ON e.salary BETWEEN s.losalary AND s.hisalary 



-- 3、确定查询的列
-- 查询列：显示经理姓名，工资，职务名称，部门名称，工资等级
-- 查询条件：根据职位为“经理”
SELECT e.ename 经理姓名,e.salary 工资,j.jname 职务名称,d.dname 部门名称,s.grade 工资等级 FROM emp e
 INNER JOIN job j ON e.job_id = j.id
 INNER JOIN dept d ON e.dept_id = d.id
 INNER JOIN salarygrade s ON e.salary BETWEEN s.losalary AND s.hisalary 
 WHERE j.jname='经理';
 
 -- 得到结论：表连接条件的个数=连接表的数量-1;
 
 /********************多表查询综合案例：练习3 *********************/
 
 -- 查询部门编号、部门名称、部门位置、部门人数
 -- 1、确定查询哪些表
-- 分析：部门编号、部门名称、部门位置： dept表
--       部门人数 : emp表， 分组统计出人数
-- 2、确定连接条件      
SELECT * FROM emp e
 INNER JOIN dept d ON e.dept_id = d.id

-- 3、确定查询的列
-- 查询列：查询部门编号、部门名称、部门位置、部门人数
-- 特殊要求：分组统计部门人数，分组条件就是部门id
SELECT d.id 部门编号,d.dname 部门名称,d.loc 部门位置,COUNT(*) 部门人数 FROM emp e
 INNER JOIN dept d ON e.dept_id = d.id
 GROUP BY e.dept_id;
 
-- 升级要求：所有部门出来，没有人数也要出来
-- 分析：外连接，根据上面部门表是右表，所以使用右外连接
SELECT d.*,COUNT(*) 部门人数 FROM emp e
 RIGHT JOIN dept d ON e.dept_id = d.id
 GROUP BY e.dept_id;
 
-- 上面使用COUNT(*) ，由于财务部没有对应员工，但是有一条部门财务部记录，所以统计出人数1
--  count(*) 无论是否有员工只统计记录数，如何解决？
-- 解决方案：使用count(e.dept_id) ，这就会统计员工dept_id不为空的记录数 
SELECT d.*,COUNT(e.dept_id) 部门人数 FROM emp e
 RIGHT JOIN dept d ON e.dept_id = d.id
 GROUP BY e.dept_id;
 
SELECT * FROM emp yuangong
 INNER JOIN emp lingdao ON yuangong.mgr=lingdao.id

-- 3、确定查询的列
-- 查询列：员工的姓名及其直接上级的姓名
SELECT yuangong.ename 员工姓名,lingdao.ename 领导姓名 FROM emp yuangong
 INNER JOIN emp lingdao ON yuangong.mgr=lingdao.id
 
 -- 要求查询所有员工，没有领导也出来，使用外连接，员工是左边，所以使用左外连接
 SELECT yuangong.ename 员工姓名,IFNULL(lingdao.ename,'BOSS') 领导姓名 FROM emp yuangong
 LEFT JOIN emp lingdao ON yuangong.mgr=lingdao.id
 -- IFNULL(lingdao.ename,'BOSS') 当前领导的名字为null，返回“BOSS”


-- 1、查询工资高于公司平均工资的员工信息
SELECT * FROM emp WHERE salary > (SELECT AVG(salary) FROM emp)

-- 2. 查询哪些表
-- 分析表：符合大于平均薪水的员工表
--         部门名称：dept表
--         上级领导：emp表
--         工资等级：salarygrade表
-- 3. 确定表连接
SELECT * FROM (SELECT * FROM emp WHERE salary > (SELECT AVG(salary) FROM emp)) highemp
 INNER JOIN dept d ON highemp.dept_id = d.id
 INNER JOIN emp lingdaoemp ON highemp.mgr=lingdaoemp.id
 INNER JOIN salarygrade s ON highemp.salary BETWEEN s.losalary AND s.hisalary;

-- 4.确定查询列
-- 查询列：员工所有信息，部门名称，上级领导，工资等级。
SELECT highemp.*,d.dname,lingdaoemp.ename,s.grade FROM (SELECT * FROM emp WHERE salary > (SELECT AVG(salary) FROM emp)) highemp
 INNER JOIN dept d ON highemp.dept_id = d.id
 INNER JOIN emp lingdaoemp ON highemp.mgr=lingdaoemp.id
 INNER JOIN salarygrade s ON highemp.salary BETWEEN s.losalary AND s.hisalary;
 
 -- 显示罗贯中，由于罗贯中没有对应的领导，所以内连接查询不出来
 -- 解决方案：使用外连接连接领导就可以解决，希望罗贯中员工出来，高工资的员工表是左边，所以使用左外连接连接领导
 SELECT highemp.*,d.dname,IFNULL(lingdaoemp.ename,'BOSS'),s.grade FROM (SELECT * FROM emp WHERE salary > (SELECT AVG(salary) FROM emp)) highemp
 INNER JOIN dept d ON highemp.dept_id = d.id
 LEFT JOIN emp lingdaoemp ON highemp.mgr=lingdaoemp.id
 INNER JOIN salarygrade s ON highemp.salary BETWEEN s.losalary AND s.hisalary;

