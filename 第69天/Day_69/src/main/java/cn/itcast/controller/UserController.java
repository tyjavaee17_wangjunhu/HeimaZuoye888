package cn.itcast.controller;

import cn.itcast.domain.Person;
import cn.itcast.domain.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.omg.CORBA.SystemException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

@Controller
@RequestMapping("/user")
public class UserController {
    //设定当前方法的映射地址
    @RequestMapping("/save")
    public ModelAndView save(ModelAndView modelAndView){
        modelAndView.setViewName("/webapp/WEB-INF/pages/index.jsp");
        modelAndView.addObject("msg","今天晚上吃什么?");
        //设定具体跳转的页面
        return modelAndView;
    }

    //前端表单只传了2.3个数据
    @RequestMapping("/method01")
    public String method01(String  address,String username){//获取表单address属性的值
        System.out.println(address);
        System.out.println(username);
        return "index.jsp";
    }

    //如果前端表单数据比较多,则将表单数据封装到实体类中比较方便
    @RequestMapping("/method02")
    public String method02(User user){//获取表单数据中address的值
        System.out.println(user);
        return "index.jsp";
    }

    //转发
    @RequestMapping("/save01")
    public String save01(){
        System.out.println("我的第一个Spring_Mvc  转发");
        return "/sucess.jsp";
    }

    //重定向
    @RequestMapping("/save02")
    public String save02(){
        System.out.println("我的第一个Spring_Mvc  重定向");
        return "redirect:/index.jsp";
    }

    //页面访问快捷设定
    @RequestMapping("/save03")
    public String save03(){
        System.out.println("我的第一个Spring_Mvc");
        return "success";
    }

    //使用modelAndView类型形参进行数据传递
    @RequestMapping("/save04")
    public ModelAndView save04(ModelAndView modelAndView) throws Exception{
        User user = new User();
        user.setName("大爹");
        user.setAge(99);
        String[] hobbies = {"抽烟,喝酒,烫头"};
        user.setHobbies(hobbies);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse("1000-10-10");
        user.setDate(date);
        modelAndView.addObject(user);
        modelAndView.setViewName("success");
        return modelAndView;
    }

    //返回JSON数据
    @RequestMapping("/save05")
    public String save05() throws Exception{
        User user = new User();
        user.setName("大爹");
        user.setAge(99);
        String[] hobbies={"抽烟","喝酒","烫头"};
        user.setHobbies(hobbies);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse("1000-10-10");
        user.setDate(date);
        ObjectMapper objectMapper = new ObjectMapper();
        String valueAsString = objectMapper.writeValueAsString(user);
        return valueAsString;
    }
}
