package cn.itcast.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import javax.xml.crypto.Data;
import java.util.Date;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String name;
    private Integer age;
    private String[] hobbies;
    private Date date;
}
