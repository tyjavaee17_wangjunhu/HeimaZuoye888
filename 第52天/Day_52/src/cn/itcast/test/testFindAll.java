package cn.itcast.test;

import cn.itcast.dao.StudentDao;
import cn.itcast.domain.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class testFindAll {
    @Test
    public void testFindAll() throws Exception {
        /*1.读取配置文件*/
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        /*2.解析配置文件*/
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        /*3.获取操作数据库的对象*/
        SqlSession sqlSession = sqlSessionFactory.openSession();
        /*4.获取代理对象*/
        StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
        /*5.调用代理对象的方法完成查询*/
        List<Student> students = studentDao.findAll();
        /*6.释放资源*/
        sqlSession.close();

        //测试查询结果
        System.out.println(students);
          }
        // @Test
        // public void testInsert()throws Exception{
        //     /*1.读取配置文件*/
        //     InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //     /*2.解析配置文件*/
        //     SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //     /*3.获取操作数据库的对象*/
        //     SqlSession sqlSession = sqlSessionFactory.openSession();
        //     /*4.获取代理对象*/
        //     StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
        //     /*5.调用代理对象的方法完成新增*/
        //     Student stu = new Student(null, "李克勤", 33);
        //     int count = studentDao.insert(stu);
        //     sqlSession.commit();
        //     /*6.释放资源*/
        //     sqlSession.close();
//
        //     //测试查询结果
        //     System.out.println("共"+count+"行记录受到影响");

        // }


        @Test
         public void testUpdate()throws Exception{
             /*1.读取配置文件*/
             InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
             /*2.解析配置文件*/
             SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
             /*3.获取操作数据库的对象*/
             SqlSession sqlSession = sqlSessionFactory.openSession();
             /*4.获取代理对象*/
             StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
             /*5.调用代理对象的方法完成更新*/
             Student stu = new Student(6, "李克清", 33);
             int count = studentDao.update(stu);
             sqlSession.commit();
             /*6.释放资源*/
             sqlSession.close();

             //测试查询结果
             System.out.println("共"+count+"行记录受到影响");
        }

    /*@Test
    public void testDelete()throws Exception{
        *//*1.读取配置文件*//*
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        *//*2.解析配置文件*//*
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        *//*3.获取操作数据库的对象*//*
        SqlSession sqlSession = sqlSessionFactory.openSession();
        *//*4.获取代理对象*//*
        StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
        *//*5.调用代理对象的方法完成删除*//*
        int count = studentDao.testDelete(1);
        sqlSession.commit();
        *//*6.释放资源*//*
        sqlSession.close();

        //测试查询结果
        System.out.println("共"+count+"行记录受到影响");
    }*/
    //}
}

