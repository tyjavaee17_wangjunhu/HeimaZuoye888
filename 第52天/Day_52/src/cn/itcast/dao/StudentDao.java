package cn.itcast.dao;

import cn.itcast.domain.Student;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface StudentDao {

    @Select("select * from student")
    List<Student> findAll();
   //@Insert(("insert into student(name,age) values #{name},#{age}"))
   //int insert(Student student);

    /*#{name},#{age},#{id}对应的是形参Student对象的属性的名字,表示把name,age,id属性的值取出来*/
    @Update("update student set name=#{name},age=#{age} where id=#{id}")
    int update(Student student);

    /*#{id}对应的是形参Student对象的属性的名字,表示把id属性的值取出来*/
    @Delete("delete from student where id = #{id}")
    int testDelete(Integer id);
}
