package cn.itcast.controller;

import cn.itcast.domain.Classes;
import cn.itcast.service.Impl.ClassesServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.mysql.jdbc.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/FindClassesByPageAndNameServlet")
public class FindClassesByPageAndNameServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            request.setCharacterEncoding("utf-8");
            response.setContentType("application/json;charset=utf-8");
            String _pageNum = request.getParameter("pageNum");
            if (StringUtil.isEmpty(_pageNum)) {
                _pageNum = "1";

                String _pageSize = request.getParameter("pageSize");
                if (StringUtil.isEmpty(_pageSize)) {
                    _pageSize = "2";
                }
                String classesName = request.getParameter("classesName");

                PageInfo<Classes> pageInfo = new ClassesServiceImpl().findByPageAndName(Integer.parseInt(_pageNum), Integer.parseInt(_pageSize), classesName);
                response.getWriter().println(new ObjectMapper().writeValueAsString(pageInfo));
            }
        }catch (Exception e){
            e.printStackTrace();
            response.getWriter().println("服务器忙");
        }
    }
}
