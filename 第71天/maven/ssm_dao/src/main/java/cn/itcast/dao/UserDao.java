package cn.itcast.dao;

import cn.itcast.domain.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserDao {
    @Insert("insert into user(id,name,password)values(#{id},#{name},#{password})")
    public boolean save(User user);
    @Update("update user set name=#{name},password=#{password} where id=#{id}")
    public boolean update(User user);
    @Delete("delete from user where id = #{id}")
    public boolean delete(Integer id);
    @Select("select * from user where id = #{id}")
    public User findById(Integer id);
    @Select("select * from user")
    public List<User> findAll(int page,int size);
    @Select("select * from user where name=#{name} and password=#{password}")
    public User login(@Param("name") String name, @Param("password") String password);
}
