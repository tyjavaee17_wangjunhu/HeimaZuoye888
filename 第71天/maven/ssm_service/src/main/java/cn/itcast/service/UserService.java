package cn.itcast.service;

import cn.itcast.domain.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserService {

    public boolean save(User user);

    public boolean update(User user);

    public boolean delete(Integer id);

    public User findById(Integer id);

    public List<User> findAll(int page,int size);

    public User login(String name,String password);
}
