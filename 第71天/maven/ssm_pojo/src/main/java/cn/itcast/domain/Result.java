package cn.itcast.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    //操作成功或失败的提示
    private boolean flag;
    //消息提示
    private String message;
    //消息状态码
    private Integer code;
    //数据
    private Object data;
}
