package cn.itcast.constant;

public class MessageConstant {
    //账户模块以1开头，长度是６
    /**查询账户成功*/
    public static final String ACCOUNT_GET_SUCCESS = "查询账户成功";
    /**查询账户失败*/
    public static final String ACCOUNT_GET_ERROR = "查询账户失败";
}
