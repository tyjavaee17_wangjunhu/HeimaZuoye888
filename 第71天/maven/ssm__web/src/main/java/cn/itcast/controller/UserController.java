package cn.itcast.controller;

import cn.itcast.constant.CodeConstant;
import cn.itcast.constant.MessageConstant;
import cn.itcast.domain.Result;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping
    public Result findAll(){
        try {
            List<User> list = userService.findAll(1, 5);
            return new Result(true, MessageConstant.ACCOUNT_GET_SUCCESS,CodeConstant.ACCOUNT_GET_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.ACCOUNT_GET_ERROR,CodeConstant.ACCOUNT_GET_ERROR,null);
        }
    }
}
