package cn.itcast.constant;

import org.apache.ibatis.annotations.Insert;

public class CodeConstant {
    //账户模块以1开头，长度是６
    /**查询账户成功*/
    public static final Integer ACCOUNT_GET_SUCCESS = 100001;
    /**查询账户失败*/
    public static final Integer ACCOUNT_GET_ERROR = 100002;
}
