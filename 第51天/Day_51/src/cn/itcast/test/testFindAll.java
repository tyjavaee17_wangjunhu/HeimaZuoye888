package cn.itcast.test;

import cn.itcast.dao.CardDao;
import cn.itcast.dao.ClassesDao;
import cn.itcast.dao.StudentDao;
import cn.itcast.domain.Classes;
import cn.itcast.domain.Student;
import jdk.internal.util.xml.impl.Input;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import javax.annotation.Resource;
import javax.smartcardio.Card;
import java.io.InputStream;
import java.sql.SQLOutput;
import java.util.List;

public class testFindAll {
 /** @Test
    public void testFindAll () throws Exception{
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        CardDao cardDao = sqlSession.getMapper(CardDao.class);
        List<Card> cards = cardDao.findAll();
        sqlSession.close();
        System.out.println(cards);
    }*/
/**@Test
public void testfindAll()throws Exception{
    InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
    SqlSession sqlSession = sqlSessionFactory.openSession();
    ClassesDao classesDao = sqlSession.getMapper(ClassesDao.class);

    List<Classes> classes = classesDao.testfindAll();

    sqlSession.close();

    System.out.println(classes);
}*/
@Test
    public void testfindAll() throws Exception {
      InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
      SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
      SqlSession sqlSession = sqlSessionFactory.openSession();
      StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
      List<Student> students = studentDao.testfindAll();
      sqlSession.close();
      System.out.println(students);

  }
}