package cn.itcast.test;

import cn.itcast.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

public class UserMapperTest2 {
    SqlSession sqlSession = null;
    @Before
    public void init()throws Exception{
        /*1.读取主配置文件*/
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");

        /*2.解析主配置文件*/
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

        /*3.创建一个操作数据的对象*/
        sqlSession = sqlSessionFactory.openSession();
    }



    @After
    public void fin(){
        sqlSession.close();
    }

    @Test
    public void save(){
        User user = new User(null, "牛宁宁", "123456", "niuningning@qq.com", "2020-09-09");
        int count = sqlSession.insert("UserMapper.save", user);
        sqlSession.commit();
    }

    @Test
    public void update(){
        User user = new User(7, "冯荣", "root", "fr@qq.com", "2020-01-09");
        int count = sqlSession.update("UserMapper.update", user);
        sqlSession.commit();
        System.out.println("共"+count+"行受到影响");
    }

    @Test
    public void delete()throws Exception{
        int count = sqlSession.delete("UserMapper.delete", 7);
        sqlSession.commit();
        System.out.println("共"+count+"行受到影响");
    }
}
