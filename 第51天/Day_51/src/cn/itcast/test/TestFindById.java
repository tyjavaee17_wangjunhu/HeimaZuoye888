package cn.itcast.test;

import cn.itcast.dao.UserDao;
import cn.itcast.domain.User;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestFindById {
    @Test
    public void testFindById() throws Exception {
        /*1.读取配置文件并创建SqlSessionFactory*/
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

        /*2.获取sqlSession[这是mybatis的重要API, 使用它可以操作数据库crud*/
        SqlSession sqlSession = sqlSessionFactory.openSession();

        /*3.由Mybatis框架创建UserDao的实现类对象，我们称为代理对象*/
        UserDao userDao = sqlSession.getMapper(UserDao.class);

        /*4.调用接口的方法*/
        User u = userDao.findById(1);

        /*5.释放资源*/
        sqlSession.close();

        //测试查询结果
        System.out.println(u);

    }

    @Test
    public void testFindByCondition() throws Exception {
        /*1.读取配置文件并创建SqlSessionFactory*/
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

        /*2.获取sqlSession[这是mybatis的重要API, 使用它可以操作数据库crud*/
        SqlSession sqlSession = sqlSessionFactory.openSession();

        /*3.由Mybatis框架创建UserDao的实现类对象，我们称为代理对象*/
        UserDao userDao = sqlSession.getMapper(UserDao.class);

        /*4.调用接口的方法*/
        User condition = new User();
        condition.setPassword("123456");
        List<User> users = userDao.findByCondition(condition);

        /*5.释放资源*/
        sqlSession.close();

        //测试查询结果
        System.out.println(users);

    }

    @Test
    public void testFindByEmail2() throws Exception {
        /*1.读取配置文件并创建SqlSessionFactory*/
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

        /*2.获取sqlSession[这是mybatis的重要API, 使用它可以操作数据库crud*/
        SqlSession sqlSession = sqlSessionFactory.openSession();

        /*3.由Mybatis框架创建UserDao的实现类对象，我们称为代理对象*/
        UserDao userDao = sqlSession.getMapper(UserDao.class);

        /*4.调用接口的方法*/
        List<User> users = userDao.findByEmail2("nglszs@qq.com", "nglszl@qq.com");
        /*5.释放资源*/
        sqlSession.close();

        //测试查询结果
        System.out.println(users);

    }
@Test
    public  void testPage() throws IOException {
    InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        PageHelper.startPage(1,2);

        UserDao userDao = sqlSession.getMapper(UserDao.class);
        List<User> users = userDao.findAll();
        sqlSession.close();

        PageInfo<User> pi = new PageInfo<>(users);
        System.out.println("总页数:" + pi.getPages());
        System.out.println("总记录数:" + pi.getTotal());
        System.out.println("每页显示条数:" + pi.getPageSize());
        System.out.println("当前第:" + pi.getPageNum());
    }
}