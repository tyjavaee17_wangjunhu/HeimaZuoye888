package cn.itcast.test;

import cn.itcast.domain.User;
import lombok.extern.log4j.Log4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;
@Log4j
public class UserMapperTest {
    @Test
    public void selectAll()throws Exception{
        /*1.读取主配置文件*/
        System.out.println(1);
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");
        System.out.println(2);
        /*2.解析主配置文件*/
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        System.out.println(3);
        /*3.创建一个操作数据的对象*/
        SqlSession sqlSession = sqlSessionFactory.openSession();
        System.out.println(4);
        /*4.执行查询*/
        List<User> list = sqlSession.selectList("UserMapper.selectAll");
        System.out.println(5);
        /*5.释放资源*/
        sqlSession.close();
        System.out.println(6);
        //测试查询结果
        System.out.println(list);
    }

    @Test
    public void selectOne() throws Exception{
        //1.读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");

        //2.解析
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

        //3.获取操作数据的对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4.查询
        User user = (User) sqlSession.selectOne("UserMapper.selectOne", 1);

        //5.释放资源
        sqlSession.close();

        //6.测试
        System.out.println(user);
    }

    @Test
    public void save()throws Exception{
        //1.读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-conﬁg.xml");

        //2.解析
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

        //3.获取操作数据的对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4.添加
        User user = new User(null, "牛宁宁", "123456", "niuningning@qq.com", "2020-09-09");
        int count = sqlSession.insert("UserMapper.save", user);

        //5.释放资源
        sqlSession.close();

        //6.测试
        System.out.println("共"+count+"行收到影响");
    }
}
