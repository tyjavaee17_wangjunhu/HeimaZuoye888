package cn.itcast.dao;


import cn.itcast.domain.User;

import java.util.List;

public interface UserDao {
    User findById(Integer id);

    List<User> findByCondition(User condition);

    List<User> findByEmail2(String... emails);

    List<User> findAll();
}