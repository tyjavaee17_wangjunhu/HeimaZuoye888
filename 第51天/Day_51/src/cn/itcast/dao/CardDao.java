package cn.itcast.dao;

import javax.smartcardio.Card;
import java.util.List;

public interface CardDao {
    List<Card> findAll();
}
