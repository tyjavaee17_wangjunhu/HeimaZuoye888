package cn.itcast.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Test;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Classes {
    private Integer id;
    private String name;
    private List<Student> students;
}
