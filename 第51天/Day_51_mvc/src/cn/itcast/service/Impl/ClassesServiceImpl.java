package cn.itcast.service.Impl;

import cn.itcast.dao.ClassesDao;
import cn.itcast.domain.Classes;
import cn.itcast.service.ClassesService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.util.List;

public class ClassesServiceImpl implements ClassesService {
    @Override
    @Test
    public PageInfo<Classes> findByPageAndName(int pageNum, int pageSize, String classesName) throws Exception {
        SqlSession sqlSession = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml")).openSession();
        ClassesDao classesDao = sqlSession.getMapper(ClassesDao.class);
        PageHelper.startPage(pageNum,pageSize);
        List<Classes> classes = classesDao.findByName(classesName);
        PageInfo<Classes> pageInfo = new PageInfo<>(classes);
        return pageInfo;
    }
}
