package cn.itcast.service;

import cn.itcast.domain.Classes;
import com.github.pagehelper.PageInfo;

public interface ClassesService {
    PageInfo<Classes> findByPageAndName(int pageNum,int pageSize,String classesName)throws Exception;
}
