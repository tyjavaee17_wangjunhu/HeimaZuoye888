package com.itheima.web.controller.system;


import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Dept;
import com.itheima.service.system.DeptService;
import com.itheima.service.system.impl.DeptServiceImpl;
import com.itheima.utils.BeanUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/system/dept")
public class DeptServlet extends BaseServlet {
    private DeptService deptService = new DeptServiceImpl();

    private void list(HttpServletRequest request, HttpServletResponse response) throws SecurityException, Exception {
        //进入列表页
        //获取数据
        int page = 1;
        int size = 5;
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (!StringUtils.isEmpty(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        PageInfo all = deptService.findAll(page, size);
        //将数据保存到指定的位置
        request.setAttribute("page",all);
        //跳转页面
        request.getRequestDispatcher("/WEB-INF/pages/system/dept/list.jsp").forward(request, response);
    }
    private void toAdd(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        //加载所有的部门信息放入到deptList
        List<Dept> all = deptService.findAll();
        request.setAttribute("deptList",all);
        //跳转页面
        request.getRequestDispatcher("/WEB-INF/pages/system/dept/add.jsp").forward(request,response);
    }

    private void save(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        //将数据获取到，封装成一个对象
        Dept dept = BeanUtil.fillBean(request,Dept.class,"yyyy-MM-dd");
        //调用业务层接口save
        deptService.save(dept);
        //跳转回到页面list
        response.sendRedirect(request.getContextPath()+"/system/dept?operation=list");
    }

    private void toEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //查询要修改的数据findById
        String id = request.getParameter("id");
        Dept dept = deptService.findById(id);
        //加载所有的部门信息放入到deptList
        List<Dept> all = deptService.findAll();
        request.setAttribute("deptList",all);
        //将数据加载到指定区域，供页面获取
        request.setAttribute("dept",dept);
        //跳转页面
        request.getRequestDispatcher("/WEB-INF/pages/system/dept/update.jsp").forward(request,response);
    }

    private void edit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //将数据获取到，封装成一个对象
        Dept dept = BeanUtil.fillBean(request,Dept.class,"yyyy-MM-dd");
        //调用业务层接口save
        deptService.update(dept);
        //跳转回到页面list
        response.sendRedirect(request.getContextPath()+"/system/dept?operation=list");
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //将数据获取到，封装成一个对象
        Dept dept = BeanUtil.fillBean(request,Dept.class);
        //调用业务层接口save
        deptService.delete(dept);
        //跳转回到页面list
        response.sendRedirect(request.getContextPath()+"/system/dept?operation=list");
    }
}
