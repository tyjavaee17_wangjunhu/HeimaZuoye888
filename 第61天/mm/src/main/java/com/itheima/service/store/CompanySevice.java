package com.itheima.service.store;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.store.Company;

import java.util.List;

public interface CompanySevice {
    /** 查询全部的数据
     * @return
     */
    List<Company> findAll();

    /**分页查询数据
     * @param page 页码
     * @param size 每页显示的数量
     */
    PageInfo findAll(int page,int size);

    /**
     *添加
     * @param company
     * @return
     */
    void save(Company company);

    /**
     *删除
     * @param company
     * @return
     */
    void delete(Company company);

    /**
     *x修改
     * @param company
     * @return
     */
    void update(Company company);

    /**
     * 查询单个
     * @param id 查询的条件(id)
     * @return 单个的结果,单个的对象
     */
    Company findById(String id);
}