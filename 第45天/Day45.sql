#1.查询工资大于5000的员工信息：员工编号，员工姓名，员工工资，部门名
SELECT
	e.emp_id '员工编号',
	e.emp_name '员工姓名',
	e.emp_wage '员工工资',
	d.dept_name '部门名'
FROM employee e,department d 
WHERE e.dept_id = d.dept_id AND e.emp_wage>5000

SELECT
	e.emp_id '员工编号',
	e.emp_name '员工姓名',
	e.emp_wage '员工工资',
	d.dept_name '部门名'
FROM employee e
LEFT JOIN department d ON e.dept_id = d.dept_id
WHERE e.emp_wage>5000

#2.查询人数大于2的部门信息：部门编号，部门名，部门人数
SELECT 
	d.dept_id '部门编号',
	d.dept_name '部门名',
	COUNT(emp_id) '部门人数'
FROM department d 
LEFT JOIN employee e ON d.dept_id = e.dept_id
GROUP BY d.dept_id
HAVING COUNT(emp_id)>2


#3.统计各部门的平均工资
SELECT
	d.dept_id,
	d.dept_name,
	IFNULL(AVG(e.emp_wage),0)
FROM department d 
LEFT JOIN employee e ON d.dept_id = e.dept_id
GROUP BY d.dept_id

#4.统计各部门的员工工资总额
SELECT
	d.dept_id,
	d.dept_name,
	IFNULL(SUM(e.emp_wage),0)
FROM department d
LEFT JOIN employee e ON d.dept_id = e.dept_id
GROUP BY d.dept_id

#5.查询工资最高的员工信息：员工编号，员工姓名，员工工资，部门名
SELECT
	*
FROM employee e 
LEFT JOIN department d ON e.dept_id = d.dept_id
WHERE e.emp_wage = (SELECT MAX(emp_wage) FROM employee)

#6.查询每个部门中工资最高的员工信息：员工编号，员工姓名，员工工资，部门名
SELECT
	d.dept_name '部门名',
	e.emp_id '员工编号',
	e.emp_name '员工姓名',
	e.emp_wage '员工工资'
FROM (
	SELECT 
		dept_id,
		MAX(emp_wage) max_wage
	FROM employee 
	GROUP BY dept_id
) t LEFT JOIN employee e ON t.dept_id = e.dept_id AND t.max_wage=  e.emp_wage
RIGHT JOIN department d ON e.dept_id = d.dept_id
