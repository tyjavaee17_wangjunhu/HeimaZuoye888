package cn.itcast.exception;

import org.omg.CORBA.SystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(RuntimeException.class)
    public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response,Object handler,Exception ex){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/error.jsp");
        modelAndView.addObject("msg","系统出错");
        return modelAndView;
    }

    @ExceptionHandler(BussinessException.class)
    @ResponseBody
    public HashMap be(Exception e){
        HashMap<Object, Object> map = new HashMap<>();
        map.put("errorMsg",e.getMessage());
        return map;
    }
    @ExceptionHandler(SystemException.class)
    @ResponseBody
    public HashMap se(Exception e){
        HashMap<Object, Object> map = new HashMap<>();
        map.put("errorMsg",e.getMessage());
        return map;
    }
}
