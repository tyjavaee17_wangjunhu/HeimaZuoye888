package cn.itcast.controller;

import cn.itcast.domain.User;
import cn.itcast.exception.BussinessException;
import org.omg.CORBA.SystemException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @RequestMapping("/ajax")
    @ResponseBody
    public Map ajax(@RequestBody List<User> users){
        HashMap<String, List<User>> map = new HashMap<>();
        map.put("1",users);
        System.out.println(map);
        return map;
    }

    @RequestMapping("method12")
    public String method12(){
        String str = null;
        System.out.println(str.endsWith("a"));
        return "sucess";
    }

    public void reg(String username,String password){
        if(StringUtils.isEmpty(username) || !username.matches("^\\d{5,12}$")){
            throw new BussinessException("账号格式6~22位字母数字组成");
        }
        if(StringUtils.isEmpty(password) || !password.matches("^[a-zA-Z0-9]{6,22}$")){

        }

        try{
            System.out.println("正在注册..");
            System.out.println(1/0);
        }catch (Exception e){
            e.printStackTrace();
          /*  throw new SystemException("注册用户时出错,联系管理员");*/
        }
    }
}