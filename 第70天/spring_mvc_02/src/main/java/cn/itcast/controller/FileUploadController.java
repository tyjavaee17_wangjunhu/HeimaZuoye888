package cn.itcast.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/file")
public class FileUploadController {
    @RequestMapping(value = "/fileUpload", produces = "application/json;charset=utf-8")
    @ResponseBody
    public String fileUpload(HttpServletRequest request, String username, String password, MultipartFile[] fileName)throws IOException{
        System.out.println("输入的用户名是:" + username);
        System.out.println("输入的密码是:" + password);
        for(MultipartFile multipartFile : fileName) {
            if(multipartFile!=null){
                String filename = multipartFile.getOriginalFilename();
                System.out.println("表单中的文件名为:" + filename);
                String realPath = request.getServletContext().getRealPath("/upload");
                multipartFile.transferTo(new File(realPath,filename));
            }
        }
        return "上传成功";
    }
}
