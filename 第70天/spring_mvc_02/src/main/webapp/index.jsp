<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<body>
<script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
<script>
    var users = [
        {
            name:"jack",
            age:34
        },
        {
            name:"tom",
            age:32
        },{
            name:"张三",
            age:3
        }


    ];
    $.ajax({
        type:"post",
        url:"/user/ajax",
        data:JSON.stringify(users),//把对象转字符串
        contentType:"application/json",//用json，text后端封装复杂数据会失败报415，自行测试
        success:function (data) {
            console.log(data)
        }
    });
</script>
</body>
</html>