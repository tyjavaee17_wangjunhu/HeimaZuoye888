package Demo.servlet.cart;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String codeInForm = request.getParameter("vcode");
        String codeInSystem = (String)request.getSession().getAttribute(CheckCode2Servlet.CODE);

        if(codeInSystem.equalsIgnoreCase(codeInForm)){
            response.getWriter().println("验证码正确");
        }else{
            response.getWriter().println("验证码错误");
        }







    }
}
