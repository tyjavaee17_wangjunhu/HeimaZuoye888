package Demo.servlet.cart;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

@WebServlet(name = "CheckCode2Servlet", urlPatterns = "/code.jpg")
public class CheckCode2Servlet extends HttpServlet {
    public static final String CODE = "cosssssdfdasfdsadfde";
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int charNum = 5;
        int width = 20 * 5;
        int height = 28;
        BufferedImage bufferedImage = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics graphics = bufferedImage.getGraphics();
        graphics.setColor(Color.YELLOW);
        graphics.fillRect(0, 0, width, height);
        graphics.setColor(Color.GRAY);
        graphics.drawRect(0, 0, width - 1, height - 1);
        graphics.setColor(Color.RED);
        graphics.setFont(new Font("宋体", Font.BOLD, 22));
        Random random = new Random();
        int n1 = random.nextInt(10);
        int n2 = random.nextInt(10);
        int m = random.nextInt(2)+1;
        String msg = "";
        String code = "";
        switch (m) {
            case 1:
                msg = String.valueOf(n1+n2);
                code=""+n1+"+"+n2+"=?";
                break;
            case 2:
                msg = String.valueOf(n1>n2?n1-n2:n2-n1);
                code=(n1>n2)?(""+n1+"-"+n2+"=?"):(""+n2+"-"+n1+"=?");
                break;
        }
        System.out.println("n1 = " + n1);
        System.out.println("n2 = " + n2);
        System.out.println("m = " + m);
        System.out.println("code = " + code);
        System.out.println("msg = " + msg);
        request.getSession().setAttribute(CODE,msg);
        int x = 5;
        for (int i = 0; i < code.length(); i++) {
            String content = String.valueOf(code.charAt(i));
            graphics.setColor(new Color(random.nextInt(255),
                    random.nextInt(255), random.nextInt(255)));
            graphics.drawString(content, x, 22);
            x += 20;
        }
        graphics.setColor(Color.GRAY);
        for (int i = 0; i < 5; i++) {
            int x1 = random.nextInt(width);
            int x2 = random.nextInt(width);
            int y1 = random.nextInt(height);
            int y2 = random.nextInt(height);
            graphics.drawLine(x1, y1, x2, y2);
        }
        graphics.dispose();
        ImageIO.write(bufferedImage, "jpg", response.getOutputStream());
    }
}