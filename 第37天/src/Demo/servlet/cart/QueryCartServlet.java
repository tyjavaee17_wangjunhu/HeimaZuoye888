package Demo.servlet.cart;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

@WebServlet("/queryCart")
public class QueryCartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Map<String,Integer> cart = (Map<String, Integer>) session.getAttribute("cart");
        if(cart!=null){
            out.print("<table border='1px' cellpadding='5px' style='border-collapse:collapse;' align='center' width='30%'>");
            out.print("<tr>");
            out.print("<th>商品名称</th>");
            out.print("<th>商品数量</th>");
            out.print("</tr>");
            Set<Map.Entry<String, Integer>> entries = cart.entrySet();
            for (Map.Entry<String, Integer> entry : entries) {
                out.print("<tr>");
                out.print("<td>"+entry.getKey()+"</td>");
                out.print("<td>"+entry.getValue()+"</td>");
                out.print("</tr>");
            }
            out.print("</table>");
        }else{
            out.print("购物车是空的");
        }
    }
}