package Demo.servlet.cart;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/addProductToCart")
public class AddProductToCartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Map<String,Integer> cart = (Map<String, Integer>) session.getAttribute("cart");
        String name = request.getParameter("name");
        Integer count = null;
        if(cart==null) {// TODO 韩
            cart = new HashMap<>();
            count = 1;
        }else {
            count = cart.get(name);
            if(count!=null) {
                count++;
            }else {
                count=1;
            }
        }
        cart.put(name,count);
        session.setAttribute("cart",cart);
        response.setContentType("text/html;charset=utf8");
        PrintWriter out = response.getWriter();
        out.print("<p>添加成功，<a href='productList.html'>继续购物</a>,<a href='queryCart'>查看购物车</a></p>");
    }
}