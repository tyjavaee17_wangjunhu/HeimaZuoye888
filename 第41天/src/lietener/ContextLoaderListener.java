package lietener;
import org.junit.Test;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ContextLoaderListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String contextConfigLocation = servletContextEvent.getServletContext().getInitParameter("contextConfigLocation");
        System.out.println(contextConfigLocation);

        if(contextConfigLocation.startsWith("classpath:")){
            try {
                InputStream is = ContextLoaderListener.class.getClassLoader().getResourceAsStream(contextConfigLocation.substring(10));
                Properties properties = new Properties();
                properties.load(is);
                System.out.println("加载配置文件完毕，配置文件中的信息如下"+properties);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    @Test
    public void xxx() {
        String str = "classpath:person.properties";
        String substring = str.substring(10);
        System.out.println(substring);

    }
}
