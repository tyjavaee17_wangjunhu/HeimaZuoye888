package cn.itcast.mapper;

import cn.itcast.domain.User;

import java.util.List;

public interface UserMapper {
    List<User> findAll()throws Exception;
    User findByUid(Integer uid)throws Exception;
}
