package cn.itcast.mapper.Impl;

import Demo.domain.user;
import cn.itcast.domain.User;
import cn.itcast.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;


import java.io.InputStream;
import java.util.List;

public class UserMapperImpl implements UserMapper {
    @Override
    public List<User> findAll() throws Exception {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        List<User> user = sqlSession.selectList("UserMapper.findAll");
        sqlSession.close();
        return user;
    }

    @Override
    public User findByUid(Integer uid) throws Exception {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        User user =sqlSession.selectOne("UserMapper.findByUid",uid);
        sqlSession.close();
        return user;
    }

    public static void main(String[] args) throws Exception {
        User user = new UserMapperImpl().findByUid(1);
        System.out.println(user);
    }
}
