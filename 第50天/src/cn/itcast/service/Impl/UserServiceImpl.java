package cn.itcast.service.Impl;

import cn.itcast.domain.User;
import cn.itcast.mapper.Impl.UserMapperImpl;
import cn.itcast.mapper.UserMapper;
import cn.itcast.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserMapper userMapper = new UserMapperImpl();

    @Override
    public List<User> findAll() throws Exception {
        return userMapper.findAll();
    }

    @Override
    public User findByUid(Integer uid) throws Exception {
        return userMapper.findByUid(uid);
    }

    public static void main(String[] args) throws Exception {
        User user = new UserServiceImpl().findByUid(1);
        System.out.println(user);
    }
}
