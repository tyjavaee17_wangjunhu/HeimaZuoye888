package cn.itcast.controller;

import cn.itcast.domain.User;
import cn.itcast.service.Impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/FindUserByUidServlet")
public class FindUserByUidServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("utf-8");
            response.setContentType("application/json;charset=utf-8");

            //接受表单数据
            String uid = request.getParameter("uid");
            //调用service
            User user = new UserServiceImpl().findByUid(Integer.parseInt(uid));
            //返回json数据
            response.getWriter().println(new ObjectMapper().writeValueAsString(user));
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
