package cn.itcast.controller;

import cn.itcast.domain.User;
import cn.itcast.service.Impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/FindAllUserServlet")
@Log4j
public class FindAllUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            log.info("访问FindAllUserServlet");
            request.setCharacterEncoding("utf-8");
            response.setContentType("application/json;charset=utf-8");

            List<User> users = new UserServiceImpl().findAll();

            String json = new ObjectMapper().writeValueAsString(users);
            response.getWriter().println(json);
            log.info("访问FindAllUserServlet成功");
        }catch (Exception e){
            e.printStackTrace();
            log.info("访问FindAllUserServlet异常");
        }
    }
}