package cn.itcast.web.servlet;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;


/**
 * @author 黑马程序员
 * 说明： 古老的数字运算的验证码图片输出
 */
@WebServlet(name = "CheckCode2Servlet", urlPatterns = "/code.jpg")
public class CheckCode2Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 使用java图形界面技术绘制一张图片
        int charNum = 5;//5个字符例如 1+2=3
        int width = 20 * 5;
        int height = 28;
        // 1. 创建一张内存图片
        BufferedImage bufferedImage = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        // 2.获得绘图对象
        Graphics graphics = bufferedImage.getGraphics();
        // 3、绘制背景颜色
        graphics.setColor(Color.YELLOW);
        graphics.fillRect(0, 0, width, height);
        // 4、绘制图片边框
        graphics.setColor(Color.GRAY);
        graphics.drawRect(0, 0, width - 1, height - 1);
        // 5、设置字体颜色和属性
        graphics.setColor(Color.RED);
        graphics.setFont(new Font("宋体", Font.BOLD, 22));
        // 随机对象
        Random random = new Random();
        int n1 = random.nextInt(10);//第一个数字
        int n2 = random.nextInt(10);//第二个数字
        int m = random.nextInt(2)+1;//运算符12代表加减
        String msg = "";// session中要用到
        String code = "";//图片中的字符
        switch (m) {
            case 1:
                msg = String.valueOf(n1+n2);
                code=""+n1+"+"+n2+"=?";
                break;//和
            case 2:
                msg = String.valueOf(n1>n2?n1-n2:n2-n1);
                code=(n1>n2)?(""+n1+"-"+n2+"=?"):(""+n2+"-"+n1+"=?");
                break;//大数减去小数的差
        }
        System.out.println("n1 = " + n1);
        System.out.println("n2 = " + n2);
        System.out.println("m = " + m);
        System.out.println("code = " + code);
        System.out.println("msg = " + msg);
        int x = 5;//坐标从5像素开始
        for (int i = 0; i < code.length(); i++) {//画验证码图片
            String content = String.valueOf(code.charAt(i));
            graphics.setColor(new Color(random.nextInt(255),
                    random.nextInt(255), random.nextInt(255)));
            graphics.drawString(content, x, 22);
            x += 20;//右移动20像素
        }
        // 6、绘制干扰线
        graphics.setColor(Color.GRAY);
        for (int i = 0; i < 5; i++) {
            int x1 = random.nextInt(width);
            int x2 = random.nextInt(width);
            int y1 = random.nextInt(height);
            int y2 = random.nextInt(height);
            graphics.drawLine(x1, y1, x2, y2);
        }
        // 释放资源
        graphics.dispose();
        // 图片输出 ImageIO
        ImageIO.write(bufferedImage, "jpg", response.getOutputStream());
    }
}