package cn.itcast.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MyAspect {
//pjp表示切入点,要增强的方法 通过对象获取方法信息
    @Around("execution(* cn.itcast.service.impl.AccountServiceImpl.findAll())")
    public Object around(ProceedingJoinPoint pjp){

        try {
            long start = System.currentTimeMillis();
            //表示执行目标方法
            Object obj = pjp.proceed(pjp.getArgs());
            System.out.println(pjp.getSignature().getName()+"执行花费了"+(System.currentTimeMillis()-start) + "毫秒");
            return obj;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }
}
