package cn.itcast.service.impl;

import cn.itcast.config.SpringConfig;
import cn.itcast.domain.Account;
import cn.itcast.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class AccountServiceImplTest {
      @Autowired
      private AccountService accountService;


    @Test
    public void findAll() {
        List<Account> all = accountService.findAll();
        System.out.println(all);
    }
}