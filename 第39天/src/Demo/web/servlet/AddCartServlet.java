package Demo.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/AddCartServlet")
public class AddCartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String name = request.getParameter("name");
        Map<String,Integer> cart = (Map<String,Integer>)request.getSession().getAttribute("cart");
        if(cart==null){
            cart=new HashMap<>();
            request.getSession().setAttribute("cart",cart);
        }

        if(cart.get(name)==null){
            cart.put(name,1);
        }else{
            int num = cart.get(name)+1;
            cart.put(name,num);
        }
        response.sendRedirect(request.getContextPath()+"/productList.jsp");
    }
}
