<%--
  Created by IntelliJ IDEA.
  User: haoyongliang
  Date: 2020/5/6
  Time: 10:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">

</head>
<body>
<h1>购物车信息</h1>


<table class="table table-striped table-bordered ">
    <thead>
    <tr>
        <th>#</th>
        <th>商品名称</th>
        <th>商品数量</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${cart}" var="entry" varStatus="vs">
            <tr>
                <th scope="row">${vs.count}</th>
                <td>${entry.key}</td>
                <td>${entry.value}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>


<%--${cart}--%>

</body>
</html>
