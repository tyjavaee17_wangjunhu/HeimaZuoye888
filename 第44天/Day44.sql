#创建学生表
CREATE TABLE student(
	id INT PRIMARY KEY AUTO_INCREMENT,
	NAME VARCHAR(24),
	age INT,
	math DOUBLE(5,2), -- 数学成绩
	chinese DOUBLE(5,2), -- 语文成绩
	english DOUBLE(5,2) -- 英语成绩
);
#插入数据
#插入数据
INSERT INTO student(NAME, age, math,chinese,english) VALUES('宁鹏耀',22,80,80,80);
INSERT INTO student(NAME, age, math,chinese,english) VALUES('皇甫芮鑫',22,0,80,8);
INSERT INTO student(NAME, age, math,chinese,english) VALUES('龙瑞',21,80,80,40);
INSERT INTO student(NAME, age, math,chinese,english) VALUES('贾香玲',21,80,90,80);
INSERT INTO student(NAME, age, math,chinese,english) VALUES('潘佳圆',21,90,84,81);
INSERT INTO student(NAME, age, math,chinese,english) VALUES('李佳媛',18,89,85,84);

SELECT 
	age,
	COUNT(*)
FROM student GROUP BY age;


SELECT 
	age '年龄',
	COUNT(*) '总人数'
FROM student
WHERE english >=60
GROUP BY age
HAVING COUNT(*)>1
ORDER BY COUNT(*) DESC
LIMIT 1

SELECT *FROM student;


SELECT * FROM student LIMIT 3;

SELECT * FROM student LIMIT 3,1; -- 跳过前三条记录 1保留一条记录

#每页显示2条记录，通过SQL语句查询每页的记录,(page-1) *pagesize 
SELECT * FROM student LIMIT 0,2; -- 第一页 0 = (1-1)*2
SELECT * FROM student LIMIT 2,2; -- 第二页 2 = (2-1)*2
SELECT * FROM student LIMIT 4,2; -- 第三页 4 = (3-1)*2


CREATE DATABASE selldb;
USER selldb;

CREATE TABLE sell(
	id INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(8) NOT NULL UNIQUE,
	sellnumbers INT NOT NULL,
	sellmoney INT NOT NULL,
	money INT NOT NULL 
);
#写出sql语句完成以上表格中数据的插入
INSERT INTO sell(`name`,sellnumbers,sellmoney,money) VALUES('郭凤芝',3,90000,8000);
INSERT INTO sell(`name`,sellnumbers,sellmoney,money) VALUES('李凤芝',1,25000,5000);
INSERT INTO sell(`name`,sellnumbers,sellmoney,money) VALUES('杨晓初',0,0,4000);
INSERT INTO sell(`name`,sellnumbers,sellmoney,money) VALUES('霍币',5,100000,6000);
INSERT INTO sell(`name`,sellnumbers,sellmoney,money) VALUES('宋明',6,12000,5000);
INSERT INTO sell(`name`,sellnumbers,sellmoney,money) VALUES('杨洋',2,50200,7000);

#写出统计sell表一共销售了多少套房源以及总销售额（sql语句）
SELECT SUM(sellmoney),SUM(money) FROM sell 

#写出计算低于平均销售额的员工姓名输出到控制台上（sql语句）【自行百度子查询】
SELECT AVG(sellmoney) FROM sell; -- 查询平均销售额
SELECT * FROM sell WHERE sellmoney<46200 -- 查询低于平均销售额的人

SELECT * FROM sell WHERE sellmoney<(SELECT AVG(sellmoney) FROM sell);
#(巨坑：子查询作为临时表的使用)写出按照销售额的降序进行排列将销售额前三名的工资分别上涨1000,800,500（sql语句）【用三条SQL语句完成，一条SQL给一个人涨工资，自行百度子查询以及错误代码】
#需求：给销售额为第一名的员工涨1000工资
#步骤1：查询销售额为第一名的员工
SELECT id FROM sell ORDER BY sellmoney DESC LIMIT 1;
#步骤2：给该员工涨工资

UPDATE sell SET money=money+1000 WHERE id = (SELECT id FROM  (SELECT * FROM sell ORDER BY sellmoney DESC LIMIT 1) t);


#SELECT id FROM  (select * from sell ORDER BY sellmoney DESC LIMIT 1) t
#删掉0销售额员工
DELETE FROM sell WHERE sellmoney=0
