package web.servlet;

import utils.CookieUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


@WebServlet(value = "/LastServlet")
public class LastServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        Cookie cookie = CookieUtils.getCookieByName("lasttime", request);
        if(cookie==null){
            response.getWriter().println("欢迎您首次访问");
        }else{
            String lasttime = cookie.getValue();
            Date date = new Date(Long.parseLong(lasttime));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
            String str = simpleDateFormat.format(date);
            response.getWriter().println("您上次访问时间是:"+str);
        }
        Cookie c = new Cookie("lasttime", String.valueOf(System.currentTimeMillis()));
        c.setPath("/");
        c.setMaxAge(60*60*24*365);
        response.addCookie(c);
    }
}
