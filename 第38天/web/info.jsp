
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
    <h1>学生信息如下</h1>
    <h6>姓名: ${cx.name}</h6>
    <h6>年龄: ${cx["age"]}</h6>
    <h6>住址: ${cx["address"]}</h6>
    <h6>Q Q: ${cx.qq}</h6>
    <h6>电话: ${cx.phone}</h6>

</body>
</html>
