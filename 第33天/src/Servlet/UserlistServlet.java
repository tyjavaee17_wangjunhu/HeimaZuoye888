package Servlet;
        import demo.User;

        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;
        import java.io.PrintWriter;
        import java.util.ArrayList;
        import java.util.List;

@WebServlet(name = "UserListServlet",urlPatterns = "/UserListServlet")
public class UserlistServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> userList = new ArrayList<>();
        userList.add(new User(100,"张三",20));
        userList.add(new User(200,"李四",22));
        userList.add(new User(300,"王五",18));
        userList.add(new User(400,"赵六",44));
        userList.add(new User(500,"六七",24));
        response.setContentType("text/html;charset=utf8");
        PrintWriter out = response.getWriter();
        out.write("<table align='center' border='1px' cellpadding='5px' cellspacing='0' width='50%'>");
        out.write("<tr>");
        out.write("<th>编号</th>");
        out.write("<th>姓名</th>");
        out.write("<th>年龄</th>");
        out.write("</tr>");
        for (User user : userList) {
            out.write("<tr>");
            out.write("<td>"+user.getId()+"</td>");
            out.write("<td>"+user.getName()+"</td>");
            out.write("<td>"+user.getAge()+"</td>");
            out.write("</tr>");
        }
        out.write("</table>");

    }
}